# ARVI - Another Risc V Implementation

O projeto consiste da implementação do RISC-V ISA I com microarquitetura single cycle. O projeto foi sintetizado e testado utilizando as FPGAS Basys e Basys3 da Digilent.

#####Tasks:

- [x] ALU
- [x] Register File
- [X] Immediate generator
- [X] Instruction Memory
- [X] Data Memory
- [X] Datapath
- [X] Controle
- [X] Vivado Packages  

##Metodologia de implementação:  
1. Implementação da componente em alto nível: descrição do componente em alto nível para se ter um modelo comportamental do mesmo a fim de produzir os test benches;  
2. Confecção do testbench: testbench para testar o módulo e sua implementação;  
3. Síntese do componente: verificar se o componente é implementável em FPGA;  

## Diretórios
O sistema de diretórios de divide em duas grandes pastas:

#### rtl/
Pasta contendo o código sintetizável com defines para síntese, módulos utilizados e top.

- rtl/modules: contém os módulos que compõem o Datapath e o próprio DataPath divididos em pastas

- rtl/top: contém um arquivo verilog que é responsável por juntar e conectar as partes(Datapath e memórias) em uma implmentação em que as memórias são separadas do datapath.

#### test/
Pasta contendo os testbenches utilizados no projeto.


- test/verilog: Possui os TestBenches de cada módulo que compõe o DataPath e o próprio DataPath divididos em pastas.

- test/TOP : Possui o TestBench do módulo TOP.


## Para contribuir:

1. Os Inputs e Outputs devem ser sinalizados com uma letra e um '_' antes do nome do sinal. Ex.: i_Addr, o_Rd;
2. O sinal de clock deve ser sempre escrito da seguinte forma: i_clk;
3. As memórias devem ser carregadas via parâmetros.

A Dedel cadeira's project. All direitos reservados.
