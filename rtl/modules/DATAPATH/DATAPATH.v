/*
	File containing a DATAPATH module to a single-cycle RISC-V processor.
*/

`timescale 1ns / 1ps

`include "defines.vh"

module DATAPATH(
`ifdef SIMULATION
	input i_clk
`endif
`ifdef FPGA
	output reg [7:0] led,
	input clk
`endif
	);

`ifdef FPGA
	wire   i_clk;
	assign i_clk = clk;
`endif

	reg  [`WORD_SIZE:0] PC;
	wire [`WORD_SIZE:0] PC_next;

	wire [`WORD_SIZE:0] instr;
	wire [`WORD_SIZE:0] addr;

	// REGISTER_FILE 
	wire [`WORD_SIZE:0] i_Wd;
	
	wire [`WORD_SIZE:0] Rd1;
	wire [`WORD_SIZE:0] Rd2;

	// IMM_GEN
	wire [`WORD_SIZE:0] Imm;

	// ALU_CONTROL 

	// ALU Input
	wire [3:0] alu_control_lines;
	wire [`WORD_SIZE:0] val1;
	wire [`WORD_SIZE:0] val2;
	wire [`WORD_SIZE:0] alu_res;

	// Flags
	wire Z;

	// DATA MEMORY
	wire [`WORD_SIZE:0] DM_Addr;
	wire [`WORD_SIZE:0] DM_ReadData;

	// CONTROL 
	wire Branch;
	wire MemRead;
	wire MemtoReg;
	wire [1:0] ALUOp;
	wire MemWrite;
	wire ALUSrc;
	wire RegWrite;

	// Wires rename
	wire [6:0] opcode;
	wire [2:0] f3;
	wire [6:0] f7;

`ifdef FPGA
	always @(posedge i_clk) begin 
		if(DM_Addr == 0 && MemWrite) begin
			led <= Rd2[7:0];
		end
	end
`endif

	assign opcode = instr[6:0];
	assign f3 = instr[14:12];
	assign f7 = instr[31:25];

	initial begin
		PC = 0;
	end

	always@(posedge i_clk) begin
		PC <= PC_next;
	end

	assign addr = PC;

	INSTRUCTION_MEMORY #(
		.HEIGHT(`INSTRUCTION_MEMORY_SIZE)
		)
		instr_mem (
		.o_Instruction(instr),
	    .i_Addr(addr)
	);
	
	REGISTER_FILE reg_file (
    	.o_Rd1(Rd1), 
    	.o_Rd2(Rd2), 
    	.i_Rnum1(instr[19:15]), 
    	.i_Rnum2(instr[24:20]), 
    	.i_Wen(RegWrite), 
    	.i_Wnum(instr[11:7]), 
    	.i_Wd(i_Wd), 
    	.i_clk(i_clk)
    );

	IMM_GEN  imm_gen (
		.i_instr(instr),
		.o_ext(Imm)
	);

	ALU_CONTROL alu_control (
		.o_ALUControlLines (alu_control_lines),
		.i_Funct7          (f7),
		.i_Funct3          (f3),
		.i_ALUOp           (ALUOp) // Using module CONTROL to generate signals
	);

	assign val1 = Rd1;

	ALU alu (
		.i_op(alu_control_lines),
		.i_Ra(val1),
		.i_Rb(val2),
		.o_Z(Z),
		.o_Rc(alu_res)
	);

	assign DM_Addr = alu_res[`WORD_SIZE:0];

//`ifdef SIMULATION
	DATA_MEMORY data_mem (
		.o_Rd   (DM_ReadData),
		.i_Wd   (Rd2),
		.i_Addr (DM_Addr),
		.i_Wen  (MemWrite),
		//.i_Ren  (MemRead),
		.i_clk  (i_clk)
	);
//`endif

	MAIN_CONTROL main_control (
		.o_Branch   (Branch),
		.o_MemRead  (MemRead),
		.o_MemWrite (MemWrite),
		.o_MemToReg (MemtoReg),
		.o_ALUOp    (ALUOp),
		.o_ALUSrc   (ALUSrc),
		.o_RegWrite (RegWrite),
		.i_OPCode   (opcode)
	);

	/*----- Datapath Muxes -----*/
	// PC Mux - BEQ
	assign PC_next = (Branch && Z) ? (PC + $signed(Imm<<1)) : PC + 4;
	// Val2 Mux
	assign val2 = ALUSrc ? Imm : Rd2;
	// MemtoReg Mux
	assign i_Wd = MemtoReg ? DM_ReadData : alu_res[`WORD_SIZE:0];
	
endmodule