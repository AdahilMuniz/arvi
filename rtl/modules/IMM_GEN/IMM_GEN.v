`timescale 1ns / 1ps

// Criar um header de defines e importá-lo para remover os defines em arquivos locais
`include "defines.vh"
//`define INSTRUCTION_SIZE 31
`define EXTENSION_SIZE 31

// Instruction types
`define R_TYPE (51) 	 // Não há imediato
`define I_TYPE_LW (3) 	 // 3 para LW19 para outras instruções de imediato
`define I_TYPE_DATA (19) // 19 para data instructions
`define S_TYPE (35) 	 // Store
`define B_TYPE (99) 	 // Branches

module IMM_GEN(
	input [`INSTRUCTION_SIZE:0] i_instr,
	output [`EXTENSION_SIZE:0] o_ext
    );

	wire [6:0]op;
	reg [11:0]imm;
	
	assign op = i_instr[6:0];

	always@(*) begin
		case(op) 
			`I_TYPE_LW:   imm = i_instr[31:20];
			`I_TYPE_DATA: imm = i_instr[31:20];
			`S_TYPE: 	  imm = {i_instr[31:25], i_instr[11:7]};
			`B_TYPE: 	  imm = {i_instr[31], i_instr[7], i_instr[30:25], i_instr[11:8]};
			default: 	  imm = 12'bx;
		endcase 
	end 

	assign o_ext = { {`EXTENSION_SIZE - 11{imm[11]}}, imm[11:0]};

endmodule
