`timescale 1ns / 1ps

`include "defines.vh"

module INSTRUCTION_MEMORY(
    output [`INSTRUCTION_SIZE:0] o_Instruction,
    input  [`WORD_SIZE:0] i_Addr
    );

	parameter HEIGHT = 256;//Memory height
	parameter FILE = "C:/Users/LaNoC/Documents/Adahil/Faculdade/SEDR/Projeto/Vivado_Project_2/test.r32i";

	reg [7:0] mem [HEIGHT-1:0];//Memory: Word: 1byte

	initial begin
		$readmemh(FILE, mem);//Initialize Memory
	end

	assign o_Instruction = {mem[i_Addr+3], mem[i_Addr+2], mem[i_Addr+1], mem[i_Addr]};//One instruction has 32 bits


endmodule
