# Make file

# Compilador
COM = iverilog
# Opções
OPT = -Wall -o
# Simulador
SIM = vvp
# Wave viewer
WV = gtkwave
# Arquivo a ser testado sem sua extensão
FILE = ALU_tb

# Compila o arquivo definido.
comp: rtl
	$(COM) $(OPT) design.vvp rtl/modules/ALU/*

# Roda o test bench do arquivo selecionado
test: rtl test/verilog/ALU/*
	$(COM) $(OPT) $(FILE).vvp rtl/modules/ALU/* test/verilog/ALU/$(FILE).v
	$(SIM) $(FILE).vvp -lxt2

# Roda o test bench e abre o gtkwave com o arquivo selecionado.
view: test
	setsid $(WV) $(FILE).vcd > /dev/null 2>&1 &

clean:
	rm $(FILE).vvp $(FILE).vcd