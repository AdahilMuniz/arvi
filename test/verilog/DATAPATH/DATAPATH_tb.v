/*
	DATAPATH_tb.v
	
	Testbench for DATAPATH.v 
*/

`timescale 1ns / 1ps

`include "rtl/defines.vh"

module DATAPATH_tb;
	/*----- Local Parameters -----*/

	/*----- Variables -----*/
	reg clk;

	/*----- DUT instatiantion -----*/
`ifdef SIMULATION
	DATAPATH dut (.i_clk(clk));
`endif
`ifdef FPGA
	wire [7:0] leds;
	DATAPATH dut (.CLK1(clk), .Led(leds));
`endif
	/*----- Functions -----*/

	/*----- Tasks -----*/

	/*----- Tests -----*/
	always
		#5 clk = ~clk;

	initial begin
		clk = 0;

		#250;
		$finish;
	end // initial


endmodule